package pl.edu.pwsztar;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ShoppingCartTest {
    private ShoppingCart cart;
    @BeforeEach
    void prepare(){
        cart = new ShoppingCart();
        cart.addProducts("Item1",10,10);
        cart.addProducts("Item2",5,5);
    }

    @ParameterizedTest(name = "shoud be able to add item {0} of price {1} and quantity {2}")
    @CsvSource({
            "Item1,10,10",
            "Item2,5,10",
            "Item3,4,3",
            "Item1,10,3",
    })
    void normalAddingTest(String itemName,int price, int quantity){
        assertTrue(cart.addProducts(itemName,price,quantity));
    }

    @ParameterizedTest(name = "Adding item {0} of price {1} shoud return {3}")
    @CsvSource({
            "Item1,3,10",
            "Item1,3,10",
            "Item2,4,1",
            "Item2,1,1",
    })
    void addingWrongPricedItemsTest(String itemName,int price,int quantity){
        assertFalse(cart.addProducts(itemName,price,quantity));
    }
    @ParameterizedTest(name = "Shoud Delete {1} of {0}")
    @CsvSource({
            "Item1,2",
            "Item2,1",
            "Item1,11",
    })
    void deletingOkItems(String itemName,int quantity){
        assertTrue(cart.deleteProducts(itemName,quantity));
    }

    @ParameterizedTest(name = "Shoud not be able to delete {1} of {0}")
    @CsvSource({
            "Item1,0",
            "Item3,1",
            "Item2,-3",
    })
    void deletingWrongItems(String itemName,int quantity){
        assertFalse(cart.deleteProducts(itemName,quantity));
    }

    @ParameterizedTest(name = "Shoud return {3} of {0}")
    @CsvSource({
            "Item1,10,10,20",
            "Item2,5,10,15",
    })
    void gettingQuantityTest(String itemName,int price, int quantity, int expected){
        cart.addProducts(itemName,price,quantity);
        assertEquals(expected,cart.getQuantityOfProduct(itemName));
    }
    @Test
    @DisplayName("[Getting Total Price] Shoud return total price of 125")
    void gettingSumPricesTest(){
        assertEquals(125,cart.getSumProductsPrices());
    }

    @ParameterizedTest(name = "Shoud return {1} for {0}")
    @CsvSource({
            "Item1,10",
            "Item2,5",
    })
    void getProductPriceTest(String itemName,int expected){
        assertEquals(expected,cart.getProductPrice(itemName));
    }

    @Test
    @DisplayName("Shoud Return List of products")
    void getProductsNamesTest(){
        List<String> expected = Arrays.asList("Item1","Item2");
        assertEquals(expected,cart.getProductsNames());
    }


}
