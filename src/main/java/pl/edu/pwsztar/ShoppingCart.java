package pl.edu.pwsztar;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {

    List<CartItem> cartContents = new LinkedList<CartItem>();

    public boolean addProducts(String productName, int price, int quantity) {
        if(quantity<1){
            return false;
        }
        int pos = CartUtils.findItem(productName,cartContents);
        if(pos != -1){
            if(cartContents.get(pos).getPrice()!=price){
                return false;
            } else {
                cartContents.get(pos).setQuantity(cartContents.get(pos).getQuantity()+quantity);
                return true;
            }
        } else {
            cartContents.add(new CartItem(productName,price,quantity));
            return true;
        }
    }

    public boolean deleteProducts(String productName, int quantity) {
        if(quantity<1){
            return false;
        }
        int pos = CartUtils.findItem(productName,cartContents);
        if(pos == -1){
            return false;
        }
        cartContents.get(pos).setQuantity(cartContents.get(pos).getQuantity()-quantity);
        if(cartContents.get(pos).getQuantity()<1){
            cartContents.remove(pos);
        }
        return true;
    }

    public int getQuantityOfProduct(String productName) {
        return CartUtils.countProducts(productName,cartContents);
    }

    public int getSumProductsPrices() {
        return cartContents.stream().mapToInt(item -> item.getPrice()*item.getQuantity()).sum();
    }

    public int getProductPrice(String productName) {
        int pos = CartUtils.findItem(productName,cartContents);
        if (pos==-1) {
            return 0;
        }
        return cartContents.get(pos).getPrice();
    }

    public List<String> getProductsNames() {
        return cartContents.stream().map(CartItem::getProductName).collect(Collectors.toList());
    }
}
