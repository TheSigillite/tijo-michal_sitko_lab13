package pl.edu.pwsztar;

import java.util.List;

public class CartUtils {
    public static int findItem(String itemName, List<CartItem> cart){
        int pos = 0;
        for(CartItem item: cart){
            if(item.getProductName().equals(itemName)){
                return pos;
            }
            pos++;
        }
        return -1;
    }
    public static int countProducts(String itemName, List<CartItem> cart){
        for (CartItem item: cart){
            if(item.getProductName().equals(itemName)){
                return item.getQuantity();
            }
        }
        return 0;
    }
}
